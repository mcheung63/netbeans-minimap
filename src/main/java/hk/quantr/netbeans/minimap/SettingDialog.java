/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package hk.quantr.netbeans.minimap;

import java.awt.Color;
import javax.swing.JColorChooser;
import javax.swing.text.JTextComponent;
import org.openide.util.NbPreferences;

/**
 *
 * @author peter
 */
public class SettingDialog extends javax.swing.JDialog {
	
	JTextComponent jtc;

	/**
	 * Creates new form SettingDialog
	 */
	public SettingDialog(java.awt.Frame parent, boolean modal, JTextComponent jtc) {
		super(parent, modal);
		this.jtc = jtc;
		initComponents();
		initValues();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        widthSpinner = new javax.swing.JSpinner();
        saveButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        marginSpinner = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        scaleSpinner = new javax.swing.JSpinner();
        ignoreMiddleWhiteSpaceCheckBox = new javax.swing.JCheckBox();
        highlightColorLabel = new javax.swing.JLabel();
        chooseHighlightColorButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        showCurrentLineCheckBox = new javax.swing.JCheckBox();
        greyColorCheckBox = new javax.swing.JCheckBox();
        bookmarkColorLabel = new javax.swing.JLabel();
        chooseBookmarkColorButton = new javax.swing.JButton();
        showLinesCheckBox = new javax.swing.JCheckBox();
        highlightOccurrenceCheckBox = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        noOfLineLowResTextField = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();

        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.jLabel6.text")); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.jLabel1.text")); // NOI18N

        widthSpinner.setModel(new javax.swing.SpinnerNumberModel(100, 20, 1000, 10));

        org.openide.awt.Mnemonics.setLocalizedText(saveButton, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.saveButton.text")); // NOI18N
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.jLabel2.text")); // NOI18N

        marginSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 0, 5, 1));

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.jLabel3.text")); // NOI18N

        scaleSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 5, 1));

        org.openide.awt.Mnemonics.setLocalizedText(ignoreMiddleWhiteSpaceCheckBox, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.ignoreMiddleWhiteSpaceCheckBox.text")); // NOI18N

        highlightColorLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        org.openide.awt.Mnemonics.setLocalizedText(highlightColorLabel, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.highlightColorLabel.text")); // NOI18N
        highlightColorLabel.setOpaque(true);

        org.openide.awt.Mnemonics.setLocalizedText(chooseHighlightColorButton, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.chooseHighlightColorButton.text")); // NOI18N
        chooseHighlightColorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseHighlightColorButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(resetButton, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.resetButton.text")); // NOI18N
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(showCurrentLineCheckBox, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.showCurrentLineCheckBox.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(greyColorCheckBox, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.greyColorCheckBox.text")); // NOI18N

        bookmarkColorLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        org.openide.awt.Mnemonics.setLocalizedText(bookmarkColorLabel, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.bookmarkColorLabel.text")); // NOI18N
        bookmarkColorLabel.setOpaque(true);

        org.openide.awt.Mnemonics.setLocalizedText(chooseBookmarkColorButton, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.chooseBookmarkColorButton.text")); // NOI18N
        chooseBookmarkColorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseBookmarkColorButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(showLinesCheckBox, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.showLinesCheckBox.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(highlightOccurrenceCheckBox, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.highlightOccurrenceCheckBox.text")); // NOI18N
        highlightOccurrenceCheckBox.setActionCommand(org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.highlightOccurrenceCheckBox.actionCommand")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.jLabel4.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.jLabel5.text")); // NOI18N

        noOfLineLowResTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("###0"))));
        noOfLineLowResTextField.setText(org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.noOfLineLowResTextField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel7, org.openide.util.NbBundle.getMessage(SettingDialog.class, "SettingDialog.jLabel7.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(highlightColorLabel)
                    .addComponent(bookmarkColorLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scaleSpinner, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(marginSpinner)
                    .addComponent(widthSpinner)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(resetButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(saveButton))
                            .addComponent(showLinesCheckBox)
                            .addComponent(ignoreMiddleWhiteSpaceCheckBox)
                            .addComponent(chooseHighlightColorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showCurrentLineCheckBox)
                            .addComponent(greyColorCheckBox)
                            .addComponent(chooseBookmarkColorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(highlightOccurrenceCheckBox)
                            .addComponent(jLabel5)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(noOfLineLowResTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(showLinesCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(widthSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(marginSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(scaleSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ignoreMiddleWhiteSpaceCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(highlightColorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chooseHighlightColorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(showCurrentLineCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(greyColorCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chooseBookmarkColorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bookmarkColorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(highlightOccurrenceCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(noOfLineLowResTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveButton)
                    .addComponent(resetButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
		NbPreferences.forModule(MinimapPanel.class).put("showLines", String.valueOf(showLinesCheckBox.isSelected()));
		NbPreferences.forModule(MinimapPanel.class).put("width", widthSpinner.getValue().toString());
		NbPreferences.forModule(MinimapPanel.class).put("margin", marginSpinner.getValue().toString());
		NbPreferences.forModule(MinimapPanel.class).put("scale", scaleSpinner.getValue().toString());
		NbPreferences.forModule(MinimapPanel.class).put("middleWhiteSpace", String.valueOf(ignoreMiddleWhiteSpaceCheckBox.isSelected()));
		String hex = "#" + Integer.toHexString(highlightColorLabel.getBackground().getRGB()).substring(2);
		NbPreferences.forModule(MinimapPanel.class).put("highlightColor", hex);
		NbPreferences.forModule(MinimapPanel.class).put("showCurrentLine", String.valueOf(showCurrentLineCheckBox.isSelected()));
		NbPreferences.forModule(MinimapPanel.class).put("highlightOccurrence", String.valueOf(highlightOccurrenceCheckBox.isSelected()));
		NbPreferences.forModule(MinimapPanel.class).put("greyColor", String.valueOf(greyColorCheckBox.isSelected()));
		NbPreferences.forModule(MinimapPanel.class).put("noOfLineLowRes", noOfLineLowResTextField.getText());
		setVisible(false);
    }//GEN-LAST:event_saveButtonActionPerformed

    private void chooseHighlightColorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseHighlightColorButtonActionPerformed
		Color color = JColorChooser.showDialog(null, "Choose highlight color", highlightColorLabel.getBackground());
		highlightColorLabel.setBackground(color);
    }//GEN-LAST:event_chooseHighlightColorButtonActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
		try {
			NbPreferences.forModule(MinimapPanel.class).clear();
		} catch (Exception ex) {
		}
		initValues();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void chooseBookmarkColorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseBookmarkColorButtonActionPerformed
		Color color = JColorChooser.showDialog(null, "Choose bookmrk color", bookmarkColorLabel.getBackground());
		bookmarkColorLabel.setBackground(color);
    }//GEN-LAST:event_chooseBookmarkColorButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bookmarkColorLabel;
    private javax.swing.JButton chooseBookmarkColorButton;
    private javax.swing.JButton chooseHighlightColorButton;
    private javax.swing.JCheckBox greyColorCheckBox;
    private javax.swing.JLabel highlightColorLabel;
    private javax.swing.JCheckBox highlightOccurrenceCheckBox;
    private javax.swing.JCheckBox ignoreMiddleWhiteSpaceCheckBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    public javax.swing.JSpinner marginSpinner;
    private javax.swing.JFormattedTextField noOfLineLowResTextField;
    private javax.swing.JButton resetButton;
    private javax.swing.JButton saveButton;
    public javax.swing.JSpinner scaleSpinner;
    private javax.swing.JCheckBox showCurrentLineCheckBox;
    private javax.swing.JCheckBox showLinesCheckBox;
    public javax.swing.JSpinner widthSpinner;
    // End of variables declaration//GEN-END:variables

	private void initValues() {
		showLinesCheckBox.setSelected(NbPreferences.forModule(MinimapPanel.class).getBoolean("showLines", true));
		widthSpinner.setValue(NbPreferences.forModule(MinimapPanel.class).getInt("width", 100));
		marginSpinner.setValue(NbPreferences.forModule(MinimapPanel.class).getInt("margin", 1));
		scaleSpinner.setValue(NbPreferences.forModule(MinimapPanel.class).getInt("scale", 1));
		ignoreMiddleWhiteSpaceCheckBox.setSelected(NbPreferences.forModule(MinimapPanel.class).getBoolean("middleWhiteSpace", true));
		showCurrentLineCheckBox.setSelected(NbPreferences.forModule(MinimapPanel.class).getBoolean("showCurrentLine", true));
		highlightOccurrenceCheckBox.setSelected(NbPreferences.forModule(MinimapPanel.class).getBoolean("highlightOccurrence", true));
		greyColorCheckBox.setSelected(NbPreferences.forModule(MinimapPanel.class).getBoolean("greyColor", false));
		String highlightColorStr = NbPreferences.forModule(MinimapPanel.class).get("highlightColor", null);
		if (highlightColorStr == null) {
			highlightColorLabel.setBackground(jtc.getSelectionColor());
		} else {
			highlightColorLabel.setBackground(Color.decode(highlightColorStr));
		}
		String bookmarkColorStr = NbPreferences.forModule(MinimapPanel.class).get("bookmarkColor", null);
		if (bookmarkColorStr == null) {
			bookmarkColorLabel.setBackground(Color.CYAN);
		} else {
			bookmarkColorLabel.setBackground(Color.decode(bookmarkColorStr));
		}
		noOfLineLowResTextField.setText(NbPreferences.forModule(MinimapPanel.class).get("noOfLineLowRes", "1000"));
	}
}

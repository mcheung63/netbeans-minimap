/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package hk.quantr.netbeans.minimap;

import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.openide.util.NbPreferences;

public class SideBarFactory implements org.netbeans.spi.editor.SideBarFactory {

	public static ArrayList<MinimapPanel> minimapPanels = new ArrayList<MinimapPanel>();

	@Override
	public JComponent createSideBar(JTextComponent jtc) {
		MinimapPanel minimapPanel = new MinimapPanel(jtc);
		minimapPanel.setVisible(MenuAction.visible);
		minimapPanels.add(minimapPanel);
		int width = NbPreferences.forModule(MinimapPanel.class).getInt("width", 100);
		minimapPanel.setPreferredSize(new Dimension(width, 500));
		return minimapPanel;
	}
}

/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package hk.quantr.netbeans.minimap;

import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

/**
 *
 * @author peter, mcheung63@hotmail.com
 */
public class NetbeansUtil {

	public static Project getProject() {
		Lookup context = Utilities.actionsGlobalContext();
		Project project = context.lookup(Project.class);
		if (project == null) {
			Node node = context.lookup(Node.class);
			if (node != null) {
				DataObject dataObject = node.getLookup().lookup(DataObject.class);
				if (dataObject == null) {
					return null;
				}
				FileObject fileObject = dataObject.getPrimaryFile();
				project = FileOwnerQuery.getOwner(fileObject);
			}
		}

		return project;
	}
}

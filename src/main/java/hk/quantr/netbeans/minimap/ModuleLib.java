/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package hk.quantr.netbeans.minimap;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

/**
 *
 * @author peter
 */
public class ModuleLib {

	public static boolean isDebug = false;
	static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:s.S");

	public static void log(String str) {
		if (isDebug) {
			InputOutput io = IOProvider.getDefault().getIO("Netbeans minimap", false);
			io.getOut().println(sdf.format(new Date()) + " - " + str);
		}
	}

	public static void log(Object obj) {
		log(obj.toString());
	}
}

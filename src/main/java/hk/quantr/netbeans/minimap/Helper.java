/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package hk.quantr.netbeans.minimap;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.CSS;

/**
 *
 * @author peter
 */
public class Helper {

	void printElements(Element elements[], String tabs) {
		for (Element element : elements) {
			int start = element.getStartOffset();
			int end = element.getEndOffset();
			try {
				ModuleLib.log(tabs + element.getDocument().getText(start, end - start).trim());
			} catch (BadLocationException ex) {
				ex.printStackTrace();
			}
			AttributeSet as = element.getAttributes();
			ModuleLib.log(tabs + "as.count=" + as.getAttributeCount());
			Enumeration e = as.getAttributeNames();
			while (e.hasMoreElements()) {
				Object o = e.nextElement();
				//System.out.println("  o=" + o.getClass());
				if (o instanceof String) {
					String attrName = (String) o;
					ModuleLib.log(tabs + " String=" + attrName);
				} else if (o instanceof StyleConstants) {
					ModuleLib.log(tabs + " StyleConstants, FontSize=" + as.getAttribute(StyleConstants.Size));
					ModuleLib.log(tabs + " StyleConstants, FontFamily=" + as.getAttribute(StyleConstants.FontFamily));
					ModuleLib.log(tabs + " StyleConstants, Foreground=" + as.getAttribute(StyleConstants.Foreground));
				} else if (o instanceof CSS.Attribute) {
					CSS.Attribute a = (CSS.Attribute) o;
					ModuleLib.log(tabs + " CSS, " + a.toString() + " - " + a.getDefaultValue());
				} else {
					ModuleLib.log("    other=" + o.getClass());
				}
			}
			ModuleLib.log(tabs + "------------------------------------------------------------------");
			for (int x = 0; x < element.getElementCount(); x++) {
				printElements(new Element[]{element.getElement(x)}, tabs + "   ");
			}
		}
	}

	private BufferedImage toBufferedImage(Component component) {
		Dimension size = component.getSize();
		Color[] colors = {Color.red, Color.green, Color.blue,
			Color.orange, Color.yellow,
			Color.white, Color.black, Color.gray};
		byte[] reds = new byte[colors.length];
		byte[] greens = new byte[colors.length];
		byte[] blues = new byte[colors.length];
		for (int i = 0; i < colors.length; i++) {
			reds[i] = (byte) colors[i].getRed();
			greens[i] = (byte) colors[i].getGreen();
			blues[i] = (byte) colors[i].getBlue();
		}
		IndexColorModel cm = new IndexColorModel(4, colors.length, reds, greens, blues);

		BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_BYTE_INDEXED, cm);
		Graphics2D g2 = image.createGraphics();

		component.paint(g2);
		return image;
	}

	public static BufferedImage resize(BufferedImage img, int newW, int newH) {
		Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
		BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = dimg.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();

		return dimg;
	}

	void saveImage(BufferedImage image, String file) {
		try {
			ImageIO.write(image, "png", new File(file));
		} catch (Exception e) {
			ModuleLib.log(e.getMessage());
			e.printStackTrace();
		}
	}

	Color getColorFromImage(BufferedImage image, int x, int y, int w, int h) {
		HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
		for (int x1 = x; x1 < x + w && x1 < image.getWidth(); x1++) {
			for (int y1 = y; y1 < y + h && y1 < image.getHeight(); y1++) {
				int c = image.getRGB(x1, y1);
				if (c != Color.white.getRGB()) {
					Integer count = m.get(c);
					if (count == null) {
						m.put(c, 1);
					} else {
						m.put(c, count + 1);
					}
				}
			}
		}

		int dominantColor = Color.white.getRGB();
		Integer maxValue = Integer.MIN_VALUE;
		for (Map.Entry<Integer, Integer> entry : m.entrySet()) {
			Integer key = entry.getKey();
			Integer value = entry.getValue();
			if (value > maxValue) {
				maxValue = value;
				dominantColor = key;
			}
		}

		return new Color(dominantColor);
	}
}

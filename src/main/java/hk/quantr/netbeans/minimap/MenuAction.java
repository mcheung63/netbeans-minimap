/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package hk.quantr.netbeans.minimap;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Tools",
		id = "hk.quantr.netbeans.minimap.MenuAction"
)
@ActionRegistration(
		displayName = "#CTL_MenuAction"
)
@ActionReference(path = "Menu/View", position = 0)
@Messages("CTL_MenuAction=Toggle minimap")
public final class MenuAction implements ActionListener {

	public static boolean visible = true;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		visible = !visible;
		for (MinimapPanel minimapPabel : SideBarFactory.minimapPanels) {
			minimapPabel.setVisible(visible);
		}
	}
}

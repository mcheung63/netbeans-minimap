/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package hk.quantr.netbeans.minimap;

import com.sun.source.tree.ClassTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePathScanner;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.swing.text.StyledDocument;
import org.netbeans.api.java.source.CompilationInfo;
import org.openide.awt.StatusDisplayer;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;

/**
 *
 * @author peter
 */
public class MemberVisitor extends TreePathScanner<Void, Void> {

	private CompilationInfo info;
	BufferedImage bufferedImage;
	int lineHeight;
	static Color constructorColor = Color.decode("#ffffde");
	static Color methodColor = Color.decode("#edf3ff");
	static Color fieldColor = Color.decode("#edfff0");

	public MemberVisitor(CompilationInfo info, BufferedImage bufferedImage, int lineHeight) {
		this.info = info;
		this.bufferedImage = bufferedImage;
		this.lineHeight = lineHeight;
	}

	@Override
	public Void visitClass(ClassTree t, Void v) {
		Element el = info.getTrees().getElement(getCurrentPath());
		if (el == null) {
			StatusDisplayer.getDefault().setStatusText("Cannot resolve class!");
		} else {
			Graphics2D imageG = bufferedImage.createGraphics();
			TypeElement te = (TypeElement) el;
			List enclosedElements = te.getEnclosedElements();
			SourcePositions sp = info.getTrees().getSourcePositions();
			try {
				StyledDocument doc = (StyledDocument) info.getDocument();
				for (int i = 0; i < enclosedElements.size(); i++) {
					Element enclosedElement = (Element) enclosedElements.get(i);
					int start = (int) sp.getStartPosition(info.getCompilationUnit(), info.getTrees().getTree(enclosedElement));
					int end = (int) sp.getEndPosition(info.getCompilationUnit(), info.getTrees().getTree(enclosedElement));
					int startLine = NbDocument.findLineNumber(doc, start);
					int endLine = NbDocument.findLineNumber(doc, end);

					Color color = null;
					if (enclosedElement.getKind() == ElementKind.CONSTRUCTOR) {
						color = constructorColor;
						//io.getOut().println(startLine + " > " + endLine + " Constructor: " + enclosedElement.getSimpleName());
					} else if (enclosedElement.getKind() == ElementKind.METHOD) {
						color = methodColor;
						//io.getOut().println(startLine + " > " + endLine + " Method: " + enclosedElement.getSimpleName());
					} else if (enclosedElement.getKind() == ElementKind.FIELD) {
						color = fieldColor;
						//io.getOut().println(startLine + " > " + endLine + " Field: " + enclosedElement.getSimpleName());
					} else {
						//imageG.setColor(Color.yellow);
						//io.getOut().println(startLine + " > " + endLine + " Other: " + enclosedElement.getSimpleName());
					}
					if (color != null) {
						imageG.setColor(color);
						imageG.fillRect(0, startLine * lineHeight, bufferedImage.getWidth(), endLine * lineHeight);
						imageG.setColor(color.darker());
						imageG.drawRect(0, startLine * lineHeight, bufferedImage.getWidth(), endLine * lineHeight);
					}
				}
			} catch (IOException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
		return null;
	}

}
